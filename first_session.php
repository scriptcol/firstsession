<?php
/****  
Autor: Script Media 
Nombre: FirstSession
Versión: 1.0
Libreria realizada para funcionamiento en framework Codeigniter
Descripción:
Esta libreria comprueba cuando un usuario ha iniciado sesión por primera vez
Función:
en la tabla de usuario debe existir un campo de tipo numerico el cual debe ser inicializado en 0 "cero"
La función comprobar retorna el valor de este campo, si el valor del  campo es cero se cambia el valor
dejandolo en 1
****/ 


 if (!defined('BASEPATH')) exit('No permitir el acceso directo al script');

 class First_session {

 	//Constructor de la clase
 	public function __construct()
    {        
        $this->ci =& get_instance();       
    }

	// Retorna todos los datos de de un determinado usuario con respecto a su id
    public function get_user_session($id)
    {	
    	$this->ci->db->where('id',$id);        
        $query = $this->ci->db->get('usuario');  
        $data =  $query->row();          
        // $this-> update_session($data,$id); //Descomentar si se quiere actualizar recien iniciada la sesión 	 
        return   $query->row();	    	
    }


    //En esta funcion despues de iniciar sesión se compara el valor de campo de usuario, si es cero se actualiza a 1
    function update_session ($data,$id){
        if ($data->first_session == 0) {
            $dt = array(
                'first_session' => 1
            );            
            $this->ci->db->where('id', $id);
            $this->ci->db->update('usuario', $dt);       
        }
    }


    

}




